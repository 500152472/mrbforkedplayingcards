//Lab exercise 2
//Frankie goetsch

//Mike Bray
//Forked Version of Frankie's Lab 2 + Functions


#include <iostream>
#include <conio.h>

using namespace std;

enum CardRank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum CardSuit
{
	SPADES, CLUBS, HEARTS, DIAMONDS
};

struct Card
{
	CardRank rank;
	CardSuit suit;
};

//hack: Use an array 
void PrintCard(Card card) {

	string suit;
	string rank;

	switch (card.suit) {
	case SPADES: suit = "Spades";
		break;
	case CLUBS: suit = "Clubs";
		break;
	case HEARTS: suit = "Hearts";
		break;
	case DIAMONDS: suit = "Diamonds";

	}

	switch (card.rank) {
	case TWO: rank = "Two";
		break;
	case THREE: rank = "Three";
		break;
	case FOUR: rank = "FOUR";
		break;
	case FIVE: rank = "FIVE";
		break;
	case SIX: rank = "SIX";
		break;
	case SEVEN: rank = "SEVEN";
		break;
	case EIGHT: rank = "EIGHT";
		break;
	case NINE: rank = "NINE";
		break;
	case TEN: rank = "TEN";
		break;
	case JACK: rank = "JACK";
		break;
	case KING: rank = "KING";
		break;
	case QUEEN: rank = "QUEEN";
		break;
	case ACE: rank = "ACE";
		break;
	}

	std::cout << rank << " of " << suit + "\n";

}

Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else {
		return card2;
	}
}

int main()
{

	Card c1;
	c1.rank = THREE;
	c1.suit = HEARTS;

	Card c2;
	c2.rank = KING;
	c2.suit = SPADES;

	PrintCard(c1);



	PrintCard(HighCard(c1, c2));



	cout << _getch();
	return 0;
}